<?php
/**
 * @file
 * Main module file for Feature Flag module.
 */

/**
 * Feature Flag functionality; return FALSE for KILL THE THING or TRUE to allow.
 */
if (!function_exists('feature_flag')) {
  function feature_flag($feature_flag) {
    $feature_flags = feature_flag_get_modules();
    if (isset($feature_flags[$feature_flag]) && $feature_flag_items = module_invoke($feature_flags[$feature_flag], 'feature_flag')) {
      return (boolean) variable_get("feature_flag_{$feature_flag}", $feature_flag_items[$feature_flag]['value']);
    }
    watchdog('feature_flag', "Warning feature flag '%flag' doesn't exist.", array('%flag' => $feature_flag), WATCHDOG_WARNING);
    // Unimplemented flag, return NO feature flag response.
    return TRUE;
  }
}

/**
 * Implements hook_help().
 */
function feature_flag_help($path, $arg) {
  if ($path == 'admin/config/development/feature-flag') {
    return '<p>' . t('When the checkbox box is <strong>checked</strong> the
      feature flag is <em>active</em> meaning the associated functionality is
      <strong>switched on</strong>. Likewise when the checkbox is
      <strong>unchecked</strong> the feature flag is <em>inactive</em> meaning
      the associated functionality is <strong>switched off</strong>. Think of it
      like standard user_access; when <strong>checked</strong> you have access
      and when <strong>unchecked</strong> you do not have access.') . '</p>';
  }
}

/**
 * Implements hook_menu().
 */
function feature_flag_menu() {
  $items['admin/config/development/feature-flag'] = array(
    'title' => 'Feature Flag Settings',
    'description' => 'Manage Feature flags: <strong>HANDLE WITH CARE</strong>!',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('feature_flag_admin'),
    'access arguments' => array('administer feature flags'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function feature_flag_permission() {
  return array(
    'administer feature flags' => array(
      'title' => t('Administer Feature Flag functionality'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function feature_flag_theme() {
  return array(
    'feature_flag_admin' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Determine the modules that feature flags belong to.
 */
function feature_flag_get_modules() {
  $feature_flags = &drupal_static(__FUNCTION__, array());
  if (empty($feature_flags)) {
    foreach (module_implements('feature_flag') as $module) {
      $feature_flag = module_invoke($module, 'feature_flag');
      foreach ($feature_flag as $key => $value) {
        $feature_flags[$key] = $module;
      }
    }
  }
  return $feature_flags;
}

/**
 * Administration settings form.
 */
function feature_flag_admin() {
  $form = $modules = array();
  $hide_descriptions = system_admin_compact_mode();
  $module_info = system_get_info('module');
  foreach (module_implements('feature_flag') as $module) {
    $modules[$module] = $module_info[$module]['name'];
  }
  asort($modules);

  foreach ($modules as $module => $display_name) {
    if ($feature_flags = module_invoke($module, 'feature_flag')) {
      $form['feature_flag'][] = array(
        '#markup' => $module_info[$module]['name'],
        '#id' => $module,
      );
      foreach ($feature_flags as $feature_flag => $feature_flag_item) {
        $feature_flag_item += array(
          'description' => '',
          'value' => TRUE,
        );
        $form['feature_flag']["feature_flag_{$feature_flag}"] = array(
          '#type' => 'checkbox',
          '#title' => $feature_flag_item['title'],
          '#description' => theme('user_permission_description', array('permission_item' => $feature_flag_item, 'hide' => $hide_descriptions)),
          '#default_value' => variable_get("feature_flag_{$feature_flag}", $feature_flag_item['value']),
        );
      }
    }
  }
  $form = system_settings_form($form);
  $form['#theme'] = 'feature_flag_admin';
  return $form;
}

/**
 * Themes the administration form.
 */
function theme_feature_flag_admin($variables) {
  $form = $variables['form'];

  foreach (element_children($form['feature_flag']) as $key) {
    $row = array();
    if (is_numeric($key)) {
      $row[] = array(
        'data' => drupal_render($form['feature_flag'][$key]),
        'class' => array('module'),
        'id' => 'module-' . $form['feature_flag'][$key]['#id'],
      );
    }
    else {
      $row[] = array(
        'data' => drupal_render($form['feature_flag'][$key]),
        'class' => array('feature_flag'),
      );
    }
    $rows[] = $row;
  }

  $output = theme('system_compact_link');
  $output .= theme('table', array(
    'header' => array(t('Settings')),
    'rows' => $rows,
    'attributes' => array(
      'id' => 'feature_flag',
    )
  ));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Implements hook_init().
 */
function feature_flag_init() {
  // Add feature flag states to the javascript layer.
  $feature_flags = feature_flag_get_modules();
  $settings = array();
  foreach ($feature_flags as $key => $value) {
    $settings[$key] = feature_flag($key);
  }
  drupal_add_js(array('feature_flags' => $settings), 'setting');
}
