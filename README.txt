Feature Flag
------------
Allows developers to show and/ or hide various aspects of their site using *feature flags* by implementing a simple hook based system (similar to how [hook_permission()](https://api.drupal.org/hook_permission) works).  The hook can define a default value for the initial state of the *feature flag* and this can also be hardcoded in your sites settings.php file.

The module exposes a user interface for managing the current states of the defined *feature flags*, this can be found by navigating to [admin/config/development/feature-flag](/admin/config/development/feature-flag) assuming your user has been given the appropriate permissions.

Return value
------------
An array whose keys are feature flag names and whose corresponding values are arrays containing the following key-value pairs:
* **title**: The human-readable name of the feature flag to be shown on the administration page. This should be wrapped in the t() function so it can be translated.
* **description**: (optional) A description of what the feature flag does. This should be wrapped in the t() function so it can be translated.
* **value**: (optional) A default value for the feature flag; if not present then defaults to **TRUE** which allows access to the feature.

Code
--------------
Implement a feature flag hook in the appropriate module file.

```php
/**
 * Implements hook_feature_flag().
 */
function example_feature_flag() {
  return array(
    'payment_method_menu_link' => array(
      'title' => t('Payment Method Link'),
      'description' => t('Show the payment method link in the user menu.'),
      'value' => FALSE, // Don't show.
    ),
  );
}
```

Then either use it to restrict access in [hook_menu()](https://api.drupal.org/hook_menu):

```php
$items['my-account/payment-methods'] = array(
  'title' => 'Payment Methods',
  'description' => 'Viewing stuff on our payment methods page.',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('exmaple_payment_methods'),
  'access callback' => 'feature_flag',
  'access arguments' => array('payment_method_menu_link'),
);
```

> If using flags for menu access control; remember to flush your menu cache should the flag be changed!
> The module **DOES NOT** handle this for you.

Or utilise in the appropriate template file:

```php
<?php if (feature_flag('payment_method_menu_link')): ?>
  <li><?php print l(t('Payment Methods', 'my-account/payment-methods'); ?></li>
<?php endif; ?>
```

NOTES
-----
* I toyed with adding more keys to the hook response namely;
 * **force**: Ignore the value in the variable and *force* the value given in the hook (probably should *disable* the checkbox in the UI too).
 * **admin**: Allow the administration user to *pass through*, or maybe a permission (?)  If it's a permission though it probably doesn't require another key here.
